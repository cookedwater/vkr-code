import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence, pad_sequence
from torch.nn import Parameter
from torch.utils.data import DataLoader


class sumunlimited(nn.Module):
    def __init__(self, hsize=150, num_seg=10, rel_dim=32, emb_mtrx=None, rnn_type='gru', device=None):
        super().__init__()
        if device==None:
            self.device = device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        else:
            self.device = device
            
        self.num_seg = num_seg
        self.rel_embed = nn.Embedding(num_seg, rel_dim)
        
        if emb_mtrx is not None:
            input_size =  emb_mtrx.shape[1]
            self.embed = nn.Embedding(emb_mtrx.shape[0], input_size)
            self.embed.weight.data.copy_(torch.Tensor(emb_mtrx))
        else:
            input_size = 64
            self.embed = nn.Embedding(120002, 64)

        if rnn_type=='gru':
            self.rnn1 = nn.GRU(input_size=input_size, hidden_size=hsize, bidirectional=True, batch_first=True)
            self.rnn2 = nn.GRU(input_size=2*hsize, hidden_size=hsize, bidirectional=True, batch_first=True)
            
        elif rnn_type=='lstm':
            self.rnn1 = nn.LSTM(input_size=input_size, hidden_size=hsize, bidirectional=True, batch_first=True)
            self.rnn2 = nn.LSTM(input_size=2*hsize, hidden_size=hsize, bidirectional=True, batch_first=True)

        self.drep = nn.Linear(2*hsize, 2*hsize)
        self.rel_pos= nn.Linear(rel_dim, 1, False)
        self.content = nn.Linear(2*hsize, 1, False)
        self.salience = nn.Bilinear(2*hsize, 2*hsize, 1, False)
        self.novelty = nn.Bilinear(2*hsize, 2*hsize, 1, False)       
        self.bias = nn.Parameter(torch.tensor([1.]))
    
 
    def pooling(self, sentences, lens, max_len=100):
        return torch.cat([F.avg_pool1d(torch.t(sent[:length]).unsqueeze(0), min(max_len, int(length))).squeeze(-1) for length, sent in zip(lens,sentences)])

    def forward(self, x, slens, dlens):
        x = self.embed(x)
        x = self.rnn1(x)[0]
        x = self.pooling(x, slens)
        x = pad_sequence(torch.split(x, dlens), batch_first=True)
        x4rnn = pack_padded_sequence(x, dlens, True, enforce_sorted=False)

        sent_reps = self.rnn2(x4rnn)[0]
        sent_reps, _ = pad_packed_sequence(sent_reps, batch_first=True)

        doc_reps = self.pooling(x, dlens)
        d = torch.tanh(self.drep(doc_reps))

        ab = np.arange(0, max(dlens))
        rel = []
        for b in np.linspace(0, dlens, self.num_seg+1).T:
            rel.append(np.digitize(ab, b))
        rel_mat = torch.from_numpy(np.clip(np.vstack(rel)-1, a_min=0, a_max=self.num_seg-1)).to(self.device)

        probs = torch.zeros((sent_reps.size(0), sent_reps.size(1))).to(self.device)
        s = torch.zeros((sent_reps.size(0), sent_reps.size(2))).to(self.device)

        for i in range(sent_reps.size(1)): 
            i_sents = sent_reps[:, i, :].squeeze(1) 
            content = self.content(i_sents)
            salience = self.salience(i_sents, d)
            novelty = self.novelty(i_sents, torch.tanh(s))

            rel_features = self.rel_embed(rel_mat[:, i])
            rp = self.rel_pos(rel_features)

            p = torch.sigmoid(content + salience - novelty + rp + self.bias)

            s = s + p*i_sents 
            probs[:, i] = torch.t(p)
        return torch.cat([prob[:lenn] for prob, lenn in zip(probs,dlens)])



class Learner():
    def __init__(self, model, train, valid, bs=32):
        self.model = model
        self.device = model.device
        self.train_dl = DataLoader(train, batch_size=bs, shuffle=True, collate_fn=self.my_collate)
        self.valid_dl = DataLoader(valid, batch_size=bs, collate_fn=self.my_collate)

    def my_collate(self, batch):
        data = [torch.tensor(item['src']) for item in batch]
        target = [torch.tensor(item['trgt']) for item in batch]
        slens = [torch.tensor(item['lens']) for item in batch]
        dlens = [item['dlen'] for item in batch]
        return [data, target, slens, dlens]

    def prepare_batch(self, batch):
        data, target, slens, dlens = batch
        data = torch.cat(data).to(self.device)
        target = torch.cat(target).to(self.device)
        slens = torch.cat(slens).to(self.device)
        return data, target, slens, dlens

    def fit(self, epochs, lr=0.0008, report_every=1000):
        loss_func = nn.BCELoss()
        optimizer = torch.optim.Adam(self.model.parameters(),lr=lr)

        best_loss = 0.165
        for epoch in range(epochs):
            self.model.train()
            tr_loss = 0.0
            for i, batch in enumerate(self.train_dl):
                data, target, slens, dlens = self.prepare_batch(batch)
                loss = loss_func(self.model(data,slens,dlens), target.float())
                loss.backward()
                optimizer.step()
                optimizer.zero_grad()

                tr_loss += loss.item()
                if i % report_every == report_every-1:
                    print(f'{i+1}-й батч {epoch + 1}-й эпохи train loss: {tr_loss / report_every}')
                    tr_loss = 0.0

                    self.model.eval()
                    with torch.no_grad():
                        tot_loss = 0.0
                        for batch in self.valid_dl:
                            data, target, slens, dlens = self.prepare_batch(batch)
                            pred = self.model(data, slens, dlens)
                            tot_loss += loss_func(pred, target.float()).item()
                        nv = len(self.valid_dl)
                        this_loss = tot_loss/nv
                        print(f'{i+1}-й батч {epoch + 1}-й эпохи validation loss: {this_loss}') 

                    if best_loss>this_loss:
                        best_loss = this_loss
                        torch.save(self.model.state_dict(), f'{epoch}_{str(this_loss)[:6]}.pt')
                        
                    if ((epoch+1)>4) and (this_loss>0.163):
                        return
                    self.model.train()