from nltk.stem import SnowballStemmer
import re
from collections import defaultdict
from typing import Collection, Counter

class Numericalizer():
    def __init__(self, spec_tok, vocab=None, max_vocab=120000, min_freq=10, otoi=None): 
        self.vocab = vocab
        self.max_vocab = max_vocab
        self.min_freq = min_freq
        self.spec_tok = spec_tok
        self.otoi = otoi
    
    def build(self, items):
        if self.vocab is None:
            freq = Counter(p for o in items for p in o)
            self.vocab = [o for o,c in freq.most_common(self.max_vocab) if c >= self.min_freq]
            for o in self.spec_tok:
                if o in self.vocab: self.vocab.remove(o)
                self.vocab.insert(0, o)
        if self.otoi==None:
            self.otoi = defaultdict(lambda: 1,{v:k for k,v in enumerate(self.vocab)}) 
      
    def __call__(self, items):
        return [[self.otoi.get(word, 1) for word in item] for item in items]


class Rouger():
    def __init__(self, up_to=2):
        self.up_to = up_to
        self.stemmer = SnowballStemmer('russian')

    def get_rouge(self, pred_set, sum_set):
        intersection = pred_set.intersection(sum_set)
        cardinality = len(intersection)
      
        precision = cardinality / len(pred_set)
        recall = cardinality / len(sum_set)

        if precision*recall==0:
          return .0

        F1 = 2.0 * ((precision * recall) / (precision + recall))
        return F1

    def calculate_one(self, prediction: str, summary: str):
        prediction, summary = self.clean(prediction).split(), self.clean(summary).split()
        assert len(prediction) > 0
        assert len(summary) > 0
        prediction, summary = [self.stemmer.stem(word) for word in prediction], [self.stemmer.stem(word) for word in summary]

        pred_unigrams = self.get_ngrams(1, prediction)
        sum_unigrams = self.get_ngrams(1, summary)

        pred_bigrams = self.get_ngrams(2, prediction)
        sum_bigrams = self.get_ngrams(2, summary)

        r1f = self.get_rouge(pred_unigrams, sum_unigrams)
        r2f =  self.get_rouge(pred_bigrams, sum_bigrams)
        return r1f, r2f

    def clean(self, text: str):
        text = text.lower()
        for func in [re.compile('[^0-9а-я]'), re.compile('\s+')]:
            text = func.sub(text, ' ')
        return text.strip()

    def get_ngrams(self, n, tokens):
        ngrams = set()
        for i in range(len(tokens) - n + 1):
            ngrams.add(tuple(tokens[i:i + n]))
        return ngrams

    def label_one(self, sentences: list, annotation: list, trgt_num=3): 
        abstract = [self.stemmer.stem(word) for word in self.clean(' '.join(sum(annotation, []))).split()]
        abs_1grams = self.get_ngrams(1, abstract)
        abs_2grams = self.get_ngrams(2, abstract)

        sentences = [self.clean(' '.join(sent)).split() for sent in sentences]
        sent_1grams = []
        sent_2grams = []
        for sent in sentences:
            stemmed = [self.stemmer.stem(word) for word in sent]
            sent_1grams.append(self.get_ngrams(1,stemmed))
            sent_2grams.append(self.get_ngrams(2,stemmed))
        selected = []
        max_score = 0
        for _ in range(trgt_num):
            try_max_score = max_score
            cur_id = -1
            for i in range(len(sentences)):
                if i in selected:
                    continue
                try_1grams = set.union(*[sent_1grams[i] for i in selected+[i]])
                try_2grams = set.union(*[sent_2grams[i] for i in selected+[i]])
                r1f = self.get_rouge(try_1grams, abs_1grams)
                r2f = self.get_rouge(try_2grams, abs_2grams)
                score = r1f + r2f
                if score > try_max_score:
                    try_max_score = score
                    cur_id = i
            if cur_id==-1:
                return selected
            selected.append(cur_id)
            max_score = try_max_score
        return sorted(selected)